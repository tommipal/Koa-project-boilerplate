const Router = require('koa-router');
const { UserFormType, LoginFormType } = require('common');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');

const router = new Router();

class UserController extends Controller {
  static async login(ctx, next) {
    const form = new LoginFormType(ctx);
    if (await form.isValid(ctx)) {
      const user = await form.getData();
      console.log(`Successful login for: ${user.email}`);
      ctx.session.user = { id: user.id, email: user.email };

      return ctx.redirect('/');
    }

    return ctx.view('login', { form });
  }

  static async logout(ctx, next) {
    ctx.session = null;

    return ctx.redirect('/login');
  }

  static async register(ctx, next) {
    const form = new UserFormType(ctx);
    if (await form.isValid(ctx)) {
      const user = await form.getData();

      if (user) return ctx.redirect('/login');
    }

    return ctx.view('register', { form });
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.all('login',      '/login',       this.login);
    router.all('register',   '/register',    this.register);

    router.use(Authenticator.login);
    router.get('logout',     '/logout',      this.logout);

    return router;
  }
}

exports.controller = UserController;