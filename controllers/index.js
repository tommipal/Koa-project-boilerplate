const Koa = require('koa');
const Router = require('koa-router');

const { Utils } = require('common');

exports.load = function (app) {
  if (!(app instanceof Koa)) throw new Error();
  const routes = [];

  console.info(`index: Loading controllers..`);
  const controllers = Utils.requireNamespace('controllers', 'controller');
  for (let [name, controller] of Utils.iterateObject(controllers)) {
    console.info(`Loading ${name} with ${controller.router.stack.length} routes`);
    for (let layer of controller.router.stack) { //TODO: check that there are no route collisions.
      if (layer.opts.name) routes.push({ name: layer.opts.name, path: layer.path });
    }
    app.use(controller.router.routes());
    app.use(controller.router.allowedMethods());
  }

  app.context.getRoute = function (name, params = {}) {
    const route = routes.find(route => route.name === name);
    if (!route) throw new Error(`No route with name "${name}"`);
    let path = route.path;
    for (let [name, value] of Utils.iterateObject(params)) path = path.replace(`:${name}`, value);

    return path;
  }
}