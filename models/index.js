const mongoose = require('mongoose');
const { config, Utils } = require('common');

exports.load = async function () {
  const uri = `mongodb://${config.databaseHost}/${config.database}`; //TODO add authetication
  await mongoose.connect(uri);  //TODO add settings
  const connection = mongoose.connection;
  connection.on('error', console.error.bind(console, 'Mongoose connection error:'));
  connection.once('open', function () { console.info(`Mongoose connected successfully to ${uri}`); });

  const models = {
    User: require('./User')
  }

  for (let [name, model] of Utils.iterateObject(models)) {
    console.info(`Loading schema for model '${name}'`);
    model = mongoose.model(name, model);
    if (!global[name]) {
      Object.defineProperty(global, name, {
        enumerable: true,
        writable: false,
        value: model
      });
    }
  }
}