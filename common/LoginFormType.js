const FormType = require('./FormType');
const { UserService } = require('common');

class LoginFormType extends FormType {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx) {
    super();

    this.fields = [
      new this.Field({
        property: 'email',
        type: 'email',
        placeholder: 'Email',
        required: true,
      }),
      new this.Field({
        property: 'password',
        type: 'password',
        placeholder: 'Password',
        required: true
      }),
      new this.Field({
        type: 'submit',
        value: "Login"
      })
    ];

    this.validation.push(async function () {
      const email = this.property('email');
      const password = this.property('password');
      const match = await UserService.match(email, password);
      if (match === false) this.errors.push(`Invalid email or password!`);

      return match
    }.bind(this));

    this.extractValues(ctx);
  }

  /**
   * @returns {Promise<User>} User
   */
  async getData() {
    const email = this.property('email');

    return await User.findOne({ email: email });
  }
}

module.exports = LoginFormType;