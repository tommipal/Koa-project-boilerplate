const path = require('path');

module.exports = {
  // csrf: {
  // 	"invalidSessionSecretMessage": "Invalid session secret",
  // 	"invalidSessionSecretStatusCode": 403,
  // 	"invalidTokenMessage": "Invalid CSRF token",
  // 	"invalidTokenStatusCode": 403,
  // 	"excludedMethods": ["GET", "HEAD", "OPTIONS"],
  // 	"disableQuery": false
  // },
  keys: {
    session: process.env['SESSION_SECRET'] || ['session-secret'],
  },
  appRoot: __dirname.split(path.sep).slice(0, -1).join(path.sep),
  env: process.env['NODE_ENV'],
  databaseHost: process.env['DATABASE_HOST'],
  database: process.env['DATABASE'],
  databaseUser: process.env['DATABASE_USER'],
  databasePassword: process.env['DATABASE_PASSWORD'],
  session: {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    /** (number || 'session') maxAge in ms (default is 1 days) */
    /** 'session' will result in a cookie that expires when session/browser is closed */
    /** Warning: If a session cookie is stolen, this cookie will never expire */
    /** Warning#2: Stealing cookies is prohibited by the Official EU Bakery protection policy */
    maxAge: 86400000,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. default is false **/
  }
}