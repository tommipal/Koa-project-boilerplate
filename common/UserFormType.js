const FormType = require('./FormType');
const { UserService } = require('common');

class UserFormType extends FormType {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx) {
    super();

    this.fields = [
      new this.Field({
        property: 'email',
        type: 'email',
        placeholder: 'Email',
        required: true,
        validate: async function () {
          if (await User.findOne({ email: this.value })) {
            console.info(`Email ${this.value} is already reserved`);
            this.error = `Email already reserved!`;
            return false;
          } else return true;
        }
      }),
      new this.Field({
        property: 'password',
        type: 'password',
        placeholder: 'Password',
        required: true,
      }),
      new this.Field({ property: 'confirmation', type: 'password', placeholder: 'Confirm password', required: true }),
      new this.Field({ type: 'submit', value: 'Submit me!' })
    ];

    this.validation.push(function () {
      const match = (this.property('password') === this.property('confirmation'));
      if (!match) this.errors.push(`Password mismatch`);

      return match;
    }.bind(this));

    this.extractValues(ctx);
  }

  /**
   * @returns {Promise<User>}
   */
  async getData() {
    const data = {};
    for (const field of this.fields) {
      switch (field.property) {
        case 'password':
          data.password = await UserService.hashpwd(field.value);
          break;
        default:
          data[field.property] = field.value;
      }
    }
    const user = new User(data);
    try {
      await user.save();
    } catch (err) {
      console.error(err);
      return null;
    }

    return user;
  }
}

module.exports = UserFormType;