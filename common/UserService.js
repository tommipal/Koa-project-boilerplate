const bcrypt = require('bcrypt');

const saltRounds = 10;

class UserService {
  static async hashpwd(pwd) {
    return await bcrypt.hash(pwd, saltRounds);
  }

  /**
   * @param {String} email
   * @param {String} pwd
   * @returns {Promise<Boolean>} match
   */
  static async match(email, pwd) {
    const user = await User.findOne({ email: email });
    if (!user) throw new Error(`No user with email '${email}'`);

    return await bcrypt.compare(pwd, user.password);
  }
}

module.exports = UserService;