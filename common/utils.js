const config = require('./config');
const fs = require('fs');

/**
 * Courtesy of megakoresh@gmail.com
 */
class Utils {
  /**
  * Returns a generator that will iterate an object's enumerable properties, compatible with for..of loops
  * @param {*} object whose enumerable properties will be iterated
  * @returns {Generator} a generator object that conforms to the iterator protocol
  */
  static * iterateObject(object) {
    for (let key in object) {
      if (object.hasOwnProperty(key)) {
        yield [key, object[key]];
      }
    }
  }

  static requireNamespace(folderPath, namespace) {
    const modules = {};
    const path = require('path');
    const directory = path.join(config.appRoot, folderPath);

    const files = require('fs').readdirSync(directory)

    let ignoreRegex = /(index)(\.js)/;

    const dirStats = fs.statSync(folderPath); //throws if the directory doesnt exist or permission is denied

    if (!dirStats.isDirectory()) throw new Error(`${folderPath} is not a directory!`);

    function* walk(directory) {
      const files = fs.readdirSync(directory);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        if (file.match(ignoreRegex)) continue;
        const stat = fs.statSync(path.join(directory, file));
        if (stat.isDirectory()) yield* walk(path.join(directory, file));
        else yield path.join(directory, file);
      }
    }

    const iterator = walk(directory);
    let result = iterator.next();
    while (!result.done) {
      if (result.value.endsWith('.ts')) {
        result = iterator.next();
        continue;
      }
      let m = require(result.value);
      if (m[namespace]) {
        //store potentially incomplete reference containing the namespace here
        modules[path.basename(result.value, '.js')] = m;
      }
      result = iterator.next();
    }

    const moduleNames = Object.keys(modules);
    for (let moduleName of moduleNames) {
      modules[moduleName] = modules[moduleName][namespace];
    }

    return modules;
  }
}

module.exports = Utils;