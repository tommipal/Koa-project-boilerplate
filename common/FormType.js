const { Utils } = require('common');

class FormType {
  constructor() {
    this.fields = [];
    this.validation = [];
    this.errors = [];
  }

  /**
   * @param {Object} ctx Koa context
   */
  extractValues(ctx) {
    for (let [name, val] of Utils.iterateObject(ctx.request.body)) {
      const field = this.fields.find(field => field.property === name);
      if (field) field.value = val;
    }
  }

  getData() {
    throw new Error('Must be implemented by a subclass!');
  }

  /**
   * @returns {Boolean} isValid
   */
  async isValid(ctx) {
    if (ctx.method !== 'POST') return false;

    let isValid = true;
    for (const field of this.fields) {
      if (!field.value && field.required) {
        field.error = `Unfilled required field`;
        isValid = false;
      }
      if (field.validate && field.validate instanceof Function) {
        let result = field.validate();
        if (result instanceof Promise) {
          if (await result === false) isValid = false;
        }
        else if (result === false) isValid = false;
      }
      if (field.match && field.match instanceof RegExp) {
        let value = new String(field.value);
        if (!value.match(field.match)) {
          field.error = `RegExp error`;
          isValid = false;
        }
      }
      if (field.test && field.test instanceof RegExp) {
        if (!field.test.test(field.value)) {
          field.error = `RegExp error`;
          isValid = false;
        }
      }
      if (field.minLength && new String(field.value).length < field.minLength) {
        field.error = `Minimum length not met!`;
        isValid = false;
      }
      if (field.maxLength && new String(field.value).length > field.maxLength) {
        field.error = `Maximum length not met!`;
        isValid = false;
      }
    }
    if (await this.validate() === false) isValid = false;

    return isValid;
  }

  /**
   * @returns {Boolean} isValid
   */
  async validate() {
    if (this.validation.length === 0) return true;
    if (!this.validation.every(validationFunction => validationFunction instanceof Function)) throw new Error();

    let isValid = true;
    for (const validationFunction of this.validation) {
      let result = validationFunction();
      if (result instanceof Promise) result = await result;
      if (result === false) isValid = false;
    }
    return isValid;
  }

  /**
  * Returns an input element matching the requested property(?)
  * @param {String} property Property name
  * @returns {String} HTML
  */
  render(property) {
    return this.fields.find(field => field.property === property).render();
  }

  route(route) {
    this.route = route;
  }

  /**
   * @returns {String} HTML
   */
  renderRest() {
    const html = this.renderStart() + this.fields.map(field => field.render()).join(' ') + this.renderEnd();

    return html;
  }

  /**
   * @returns {String} HTML
   */
  renderStart() {
    if (!this.route) throw new Error(`Missing route for submit!`);

    return `<p>${this.errors.join(' ')}</p>
    <form action="${this.route}" method="POST">`;
  }

  /**
   * @returns {String} HTML
   */
  renderEnd() {
    return `</form>`;
  }

  get Field() { return Field; }
  static get Field() { return Field; }

  /**
   * @param {String} property
   * @returns {any} value
   */
  property(property) {
    const field = this.fields.find(field => field.property === property);
    if (!field) throw new Error();

    return field.value;
  }
}

class Field {
  constructor(config) {
    for (let [name, val] of Utils.iterateObject(config)) {
      switch (name) {
        case 'property':
        case 'type':
        case 'placeholder':
        case 'value':
        case 'minLength':
        case 'maxLength':
        case 'match':
        case 'test':
        case 'required':
        case 'validate':
          this[name] = val;
          break;
        default:
          throw new Error(`Found an unsupported field param "${name}"`);
      }
    }
  }

  render() {
    switch (this.type) {
      case 'text':
      case 'password':
      case 'email':
        let element = `<input name="${this.property}"
          type="${this.type}"
          placeholder="${this.placeholder}"
          ${this.value ? ('value="' + this.value + '"') : ''}
          ${this.required ? 'required' : ''}
          >`;
        if (this.error) element += `<p>${this.error}</p>`;
        return element;
      case 'submit':
        return `<input type="${this.type}" value="${this.value}">`;
      default:
        throw new Error(`Encountered an unkown input type "${this.type}"`);
    }
  }
}

module.exports = FormType;