const koa = require('koa');
var bodyParser = require('koa-bodyparser');
const Router = require('koa-router');
const session = require('koa-session');
const views = require('koa-views');
const { config } = require('common');
const controllers = require('controllers');
const models = require('models');
const { Responder } = require('middleware');

const app = new koa();
app.keys = config.keys.session;

const router = new Router();

(async function init() {
  await models.load();

  app.use(session(config.session, app));
  app.use(bodyParser());

  app.use(views(config.appRoot + '/views', { extension: 'ejs' }));

  // Tempopary implementation of the index action
  router.get('/', function (ctx, next) {
    return ctx.view('index', { })
  });

  app.use(Responder.handle);

  app.use(router.routes());
  controllers.load(app);

  // Extend the context's render function
  app.context.view = function (view, params = {}) {
    params.ctx = this; // Always pass the context to the view

    return this.render(view, params);
  }

  app.on('error', (err, ctx) => {
    console.error('server error', err, ctx);
  });

  app.listen(3000);
  console.info('Application running on port 3000');
})();