class Authenticator {
  /**
   * @param {Object} ctx
   * @param {Function} next
   */
  static async login(ctx, next) {
    if (!ctx.session.user) throw new Error(`Unauthorized user!`);

    return await next();
  }
}

module.exports = Authenticator;