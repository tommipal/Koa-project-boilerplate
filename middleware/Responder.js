class Responder {
  /**
   * Function as an outer try catch to handle any errors
   * @param {Object} ctx
   * @param {Function} next
   */
  static async handle(ctx, next) {
    try {
      await next();
    } catch (err) {
      console.error(err);
      return ctx.render('error', { error: err });
    }
  }
}

module.exports = Responder;